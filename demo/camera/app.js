let video
let imageCapture
document.querySelector('#start').addEventListener('click', async ev => { start() })
document.querySelector('#capture').addEventListener('click', async ev => { capture() })
document.querySelector('#stop').addEventListener('click', async ev => { stop() })


async function start() {
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        const msg = document.querySelector('#message')
        msg.innerHTML = 'media capture not supported'
        throw new Error('media capture not supported')
    }
    try {
        const stream = await navigator.mediaDevices.getUserMedia({ video: true })
        video = document.querySelector('#video')
        video.srcObject = stream
        const track = stream.getVideoTracks()[0]
        imageCapture = new ImageCapture(track)
        document.querySelector('#camera').innerHTML = track.label
    } catch (err) {
        console.error('** failed: %o', err)
    }
}

async function stop() {
    const stream = video.srcObject
    if (stream) {
        stream.getTracks().forEach(t => t.stop())
    }
    video.srcObject = null
}

async function capture() {
    const blob = await imageCapture.takePhoto()
    const photo = document.querySelector('#photo')
    photo.src = URL.createObjectURL(blob)

    const label = document.querySelector('#label')
    label.innerHTML = new Date().toLocaleString()

    const card = document.querySelector('.card')
    card.classList.remove('hidden')
}
