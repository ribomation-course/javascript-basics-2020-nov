function css(sel) {
    return document.querySelector(sel)
}

css('#geo').addEventListener('click', ev => {
    navigator.geolocation.getCurrentPosition(pos => {
        console.log('pos: %o', pos);
        css('#lat').innerText = pos.coords.latitude
        css('#lng').innerText = pos.coords.longitude
        css('#acc').innerText = pos.coords.accuracy
        css('#ts').innerText = new Date(pos.timestamp).toLocaleString()
    })
})

