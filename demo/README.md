# Demo Code

Before trying to run any of the project (*except sippets/*) do the following:

1. Make a fresh folder
1. Copy the content of a project into the fresh folder
1. Open a terminal window in the folder and run `npm install`
1. In most cases there is a script command in `package.json` you can run, like `npm run dev`
