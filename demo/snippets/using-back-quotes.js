let name = 'Anna Conda'
let sq = 'Hello ${name}, nice to meet you.'
let dq = "Hello ${name}, nice to meet you."
let bq = `Hello ${name}, nice to meet you.`

console.log('single-quote: %o', sq);
console.log('double-quote: %o', dq);
console.log('back-quote  : %o', bq);
