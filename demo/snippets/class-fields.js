class Book {
    static VAT = 25
    name = ''
    isbn = ''
    #_price = Math.random() < .5 ? 1000 : 1500

    constructor(name, isbn) {
        this.name = name
        this.isbn = isbn
    } 
    get price() { return this.#_price * (1 + Book.VAT/100) }
    toString() {
        const p = this.price.toLocaleString('sv', {
            useGrouping:true, 
            minimumFractionDigits:2, 
            maximumFractionDigits:2
        })
        return `Book{${this.name}, ${p}, ISBN ${this.isbn}}`
    }
}

let books = [
    new Book('JavaScript - The Definitive Guide, 7e', '9781491952023'),
    new Book('Clean Code in JavaScript', '9781789957648'),
    new Book('Eloquent Javascript, 3rd Edition', '9781593279509'),
]
books.forEach(b => console.log(b.toString()))

let b = books[0]
console.log('b.name: %o', b.name)
console.log('b.isbn: %o', b.isbn)
console.log('b.price: %o', b.price)


console.log('b.#_price: %o', b.#_price)


