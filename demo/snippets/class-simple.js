class Person {
    constructor(name, age = 42) {
        this._id = Person.lastId++
        this._name = name
        this._age = age
    }

    incrAge() { ++this._age }
    toString() {
        return `Person{${this._id}, ${this._name}, ${this._age}}`
    }

    static lastId = 1
    static names  = [ 'anna','berit','carin','doris','eva' ]
    static create() {
        const name = Person.names[Math.floor(Math.random() * Person.names.length)]
        const age  = Math.floor(18 + 50 * Math.random())
        return new Person(name, age)
    }
}

console.log('lastId: %s', Person.lastId);
console.log('names : %s', Person.names);

let persons = Array.from(Array(5)).map(_ => Person.create())
persons.forEach(p => console.log(p.toString()))


