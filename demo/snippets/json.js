const {writeFileSync, readFileSync} = require('fs')

let objs = [
    {name:'anna', age:42, female:true},
    {name:'berra', age:52, female:false},
    {name:'carin', age:23, female:true},
    {name:'dennis', age:37, female:false},
]

let jsonOut = JSON.stringify(objs,null,2)
writeFileSync('data.json', jsonOut, 'utf8')
let jsonIn  = readFileSync('data.json', 'utf8')
let objs2   = JSON.parse(jsonIn)
console.log('objs2: %o', objs2);
