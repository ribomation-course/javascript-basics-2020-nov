class Pet {
    #name = 'no name'
    constructor(name) { this.#name = name }
    say() { return `${this.#name} says ` }
}

class Dog extends Pet {
    constructor(name) { super(name) }
    say() { return super.say() + 'woff woff' }
}

class Cat extends Pet {
    constructor(name) { super(name) }
    say() { return super.say() + 'meow meow' }
}

let c = new Cat('Groovy')
let d = new Dog('Tessie')

console.log('c: ', c.say());
console.log('d: ', d.say());


