if (!window.getSelection || !document.createRange) {
    throw new Error('selection not supported')
}

if (!navigator.clipboard) {
    throw new Error('clipboard not supported')
}

function css(sel) {
    return document.querySelector(sel)
}

const selectionSvc = window.getSelection()

css('#select').addEventListener('click', ev => {
    if (selectionSvc.toString() === '') {
        selectionSvc.removeAllRanges()
        const range = document.createRange()
        range.selectNodeContents(css('#lorem'))
        selectionSvc.addRange(range)
    }
})

css('#deselect').addEventListener('click', ev => {
    selectionSvc.removeAllRanges()
})

css('#copy').addEventListener('click', async ev => {
    const txt = selectionSvc.toString()
    if (txt !== '') {
       try {
        await navigator.clipboard.writeText(txt)
        css('#clipboard').innerText = txt
       } catch (err) {
           console.error('failed: %o', err);
       }
    }
})


