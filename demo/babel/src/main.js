import {sum, fancySquare} from './numbers'

let arg = +(process.argv[2] || 10)
let res = sum(arg)
console.log('sum(%d) = %d', arg, res)

let ints = Array.from(Array(10).keys(), x => x+1)
console.log('ints: %o', ints)
let sq = fancySquare(ints)
console.log('sq  : %o', sq)


