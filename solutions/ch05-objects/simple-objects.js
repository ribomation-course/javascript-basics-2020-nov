const prod1 = { name: 'Apple', price: 12.3, outOfStock: false }
const prod2 = { name: 'Banana', price: 45.6, outOfStock: true }

console.log('prod1: ', prod1);
console.log('prod2: %o', prod2);

Object.keys(prod1)
    .forEach(key => console.log('%s = %s', key, prod1[key]))

Object.entries(prod2)
    .forEach(([key,val]) => console.log('%s = %s', key, val))
