function Product(name, price = 100, inStock = true) {
    this.name = name
    this.price = price
    this.inStock = inStock
}

const products = ['apple', 'banana', 'coco nut', 'date plum']
    .map(name => new Product(name.toUpperCase(), Math.random() * 100, Math.random() < .75))

products.forEach(p =>
    console.log('%s, %s kr (%s stock)',
        p.name,
        p.price.toFixed(2),
        p.inStock ? 'in' : 'out of'))

console.log('----');
Object.entries(products[0])
    .forEach(p => console.log('%s = %s', p[0], p[1]))
