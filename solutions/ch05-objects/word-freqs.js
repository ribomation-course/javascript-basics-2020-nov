const { readFileSync } = require('fs')

const filename    = process.argv[2]  || __filename
const maxWords    = +process.argv[3] || 25
const minWordSize = +process.argv[4] || 5

const wordFreqs = readFileSync(filename)
    .toString()
    .split(/[^a-z]+/i)
    .filter(w => w.length >= minWordSize)
    .map(w => w.toLowerCase())
    .reduce((freqs, word) => {
        freqs[word] = 1 + (+freqs[word] || 0)
        return freqs
    }, {})

Object.entries(wordFreqs)
    .sort( (lhs, rhs) => rhs[1] - lhs[1] )
    .slice(0, maxWords)
    .forEach(([word,count]) => console.log('%s: %d', word, count))

