function sum(n) {
    if (n <= 1) return 1
    return n + sum(n - 1)
}
function prd(n) {
    if (n <= 1) return 1
    return n * prd(n - 1)
}
function fib(n) {
    if (n <= 0) return 0
    if (n === 1) return 1
    return fib(n - 2) + fib(n - 1)
}
function table(N) {
    console.log('%s\t%s\t%s\t%s', 'k', 'sum', 'fib', 'prd')
    for (let k = 1; k <= N; ++k) {
        console.log('%d\t%d\t%d\t%d', k, sum(k), fib(k), prd(k));
    }
}

table(+process.argv[2] || 10)

//console.log(process.argv);
