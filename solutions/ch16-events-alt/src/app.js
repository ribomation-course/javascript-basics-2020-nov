
class Todo {
    text = ''
    before = new Date()
    done = false
    dom = undefined

    constructor(text, before = new Date()) {
        this.text = text
        if (before instanceof Date) {
            this.before = before
        } else if (typeof before === 'string') {
            this.before = new Date(before)
        } else {
            throw new Error('invalid date: ' + before + ', ' + typeof before)
        }
    }

    toggle() {
        this.done = !this.done
        return this.done
    }

    toDOM(idx) {
        const article = document.createElement('article')
        const self = this
        article.innerHTML = `
            <h5>
            <span>${this.before.toDateString()}</span>
            <button class="done">Done</button>
            <button class="remove">Remove</button>
            </h5>
            <p>${this.text}</p>
        `

        const doneBtn = article.querySelector('.done')
        doneBtn.addEventListener('click', ev => {
            self.toggle()
            if (self.done) self.dom.classList.add('done')
            else self.dom.classList.remove('done')
        })

        const removeBtn = article.querySelector('.remove')
        removeBtn.addEventListener('click', ev => {
            const removeEvt = new CustomEvent('remove-todo', {detail: {
                index: idx,
                dom: article
            }})
            const tasksEl = document.querySelector('#tasks')
            tasksEl.dispatchEvent(removeEvt)
        })

        this.dom = article
        return article
    }
}

let tasks = []

function add(text, before) {
    tasks.push(new Todo(text, before))
    tasks.sort((lhs, rhs) => rhs.before.getTime() - lhs.before.getTime())
}

function populate() {
    const tasksEl = document.querySelector('#tasks')
    tasksEl.innerHTML = ''
    let idx=0
    for (let task of tasks) {
        tasksEl.appendChild(task.toDOM(idx++))
    }
}


const tasksEl = document.querySelector('#tasks')
tasksEl.addEventListener('remove-todo', ev => {
    tasks.splice(ev.detail.index, 1)
    tasksEl.removeChild(ev.detail.dom)
})

const save = document.querySelector('#saveBtn')
save.addEventListener('click', ev => {
    const textFld = document.querySelector('#textFld')
    const beforeFld = document.querySelector('#beforeFld')

    if (textFld.value && beforeFld.value) {
        add(textFld.value, beforeFld.value)
        populate()
        textFld.value = beforeFld.value = ''
    }
})

add('first task', '2020-11-27')
add('second task', '2020-11-25')
add('third task', '2020-11-30')
add('se kalle anka', '2020-12-24')
populate()
