
class Todo {
    text = ''
    before = new Date()
    done = false
    dom = undefined
    
    constructor(text, before = new Date()) {
        this.text = text
        if (before instanceof Date) {
            this.before = before
        } else if (typeof before === 'string') {
            this.before = new Date(before)
        } else {
            throw new Error('invalid date: ' + before + ', ' + typeof before)
        }
    }

    toggle() {
        this.done = !this.done
        return this.done
    }

    toDOM(idx) {
        const self = this

        // build H5 tag
        const span = document.createElement('span')
        span.appendChild(document.createTextNode(self.before.toDateString()))

        const done = document.createElement('button')
        done.appendChild(document.createTextNode('Done'))

        const remove = document.createElement('button')
        remove.appendChild(document.createTextNode('Remove'))

        const h5 = document.createElement('h5')
        h5.appendChild(span)
        h5.appendChild(done)
        h5.appendChild(remove)

        // build P tag
        const p = document.createElement('p')
        p.appendChild(document.createTextNode(self.text))
       
        // top-level ARTICLE tag
        const article = document.createElement('article')
        if (self.done) article.classList.add('done')
        article.appendChild(h5)
        article.appendChild(p)

        /*
        article.innerHTML = `
        <article >
            <h5>
            <span>${self.before.toDateString()}</span>
            <button>Done</button>
            <button>Remove</button>
            </h5>
            <p>${self.text}</p>
        </article>      
        `
        */

        
        this.dom = article
        return article
    }
}

let tasks = []

function add(text, before) {
    tasks.push(new Todo(text, before))
    tasks.sort((lhs, rhs) => rhs.before.getTime() - lhs.before.getTime())
}

function populate() {
    const tasksElem = document.querySelector('#tasks')
    tasksElem.innerHTML = ''
    for (let task of tasks) {
        const dom = task.toDOM()
        tasksElem.appendChild(dom)
    }
}


add('first task', '2020-10-28')
add('second task', '2020-10-29')
add('third task', '2020-10-30')

populate()
