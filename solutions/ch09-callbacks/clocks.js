const now    = () => new Date().toISOString().substr(11, 12)
const clock  = (n) => 
    () => console.log('%s[%d ms] %s', '   '.repeat(n), 500 * 2 ** n, now())

const clocks = [...Array(3).keys()].map(n => setInterval(clock(n), 500 * 2 ** n))

setTimeout(() => {
    clocks.forEach(c => clearInterval(c))
}, 10 * 1000)

