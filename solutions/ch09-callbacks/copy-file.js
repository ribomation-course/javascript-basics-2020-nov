const {readFile, writeFile} = require('fs')
const {basename} = require('path')

const infile = process.argv[2] || __filename
const outfile = 'uppercase-of_' + basename(infile) + '.txt'

readFile(infile, 'utf8', (err, payload) => {
    if (err) {
        console.error('read failed: %o', err)
        process.exit(1)
    }

    const txt = payload.toUpperCase()

    writeFile(outfile, txt, 'utf8', (err) => {
        if (err) {
            console.error('write failed: %o', err)
            process.exit(1)
        }

        console.log('written %d bytes to %s', txt.length, basename(outfile));
    })
})

