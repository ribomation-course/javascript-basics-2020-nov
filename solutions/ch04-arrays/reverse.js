function reverse(sentence) {
    if (!sentence || sentence.trim().length === 0) return ''
    return sentence.split(/\s+/).reverse().join(' ')
}

const txt = process.argv[2] || 'Foobar Strikes Again'
console.log('%s --> %s', txt, reverse(txt))
