const { readFileSync } = require('fs')

function words(filename, max=10) {
    readFileSync(filename)
        .toString()
        .split(/[^a-z]+/i)
        .sort( (lhs, rhs) => rhs.length - lhs.length )
        .filter((word, idx, arr) => arr.indexOf(word,idx+1) === -1)
        .slice(0, max)
        .forEach(w => console.log('%s:%d', w, w.length))
}

words(process.argv[2] || __filename, +process.argv[3])

