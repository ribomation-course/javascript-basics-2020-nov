
class Todo {
    id = -1
    text = ''
    before = new Date()
    done = false
    dom = undefined

    constructor(id, text, before = new Date(), done = false) {
        this.id = id
        this.text = text
        this.done = done
        if (before instanceof Date) {
            this.before = before
        } else if (typeof before === 'string') {
            this.before = new Date(before)
        } else {
            throw new Error('invalid date: ' + before + ', ' + typeof before)
        }
    }

    toggle() {
        this.done = !this.done
        return this.done
    }

    static fromServer(json) {
        return new Todo(json.id, json.text, json.before, json.done)
    }

    toServer() {
        return Object.assign({}, this)
    }

    toDOM(idx) {
        const self = this

        // build H5 tag
        const span = document.createElement('span')
        span.appendChild(document.createTextNode(self.before.toDateString()))

        const done = document.createElement('button')
        done.classList.add('done')
        done.appendChild(document.createTextNode('Done'))
        done.addEventListener('click', ev => {
            if (self.toggle()) self.dom.classList.add('done')
            else self.dom.classList.remove('done')
        })

        const remove = document.createElement('button')
        remove.classList.add('remove')
        remove.appendChild(document.createTextNode('Remove'))
        remove.addEventListener('click', ev => {
            const parent = self.dom.parentElement
            parent.dispatchEvent(new CustomEvent('remove-todo', {
                detail: {
                    index: idx,
                    dom: self.dom
                }
            }))
        })

        const h5 = document.createElement('h5')
        h5.appendChild(span)
        h5.appendChild(done)
        h5.appendChild(remove)

        // build P tag
        const p = document.createElement('p')
        p.appendChild(document.createTextNode(self.text))

        // top-level ARTICLE tag
        const article = document.createElement('article')
        if (self.done) article.classList.add('done')
        article.appendChild(h5)
        article.appendChild(p)

        /* article.innerHTML = `<article>
            <h5>
             <span>${self.before.toDateString()}</span>
             <button>Done</button>
             <button>Remove</button>
            </h5>
            <p>${self.text}</p>
        </article>` */

        this.dom = article
        return article
    }
}

const baseUrl = 'http://localhost:3000/todos'
let tasks = []


function add(id, text, before, done = false) {
    tasks.push(new Todo(id, text, before, done))
    tasks.sort((lhs, rhs) => rhs.before.getTime() - lhs.before.getTime())
}

function populate() {
    const tasksElem = document.querySelector('#tasks')
    tasksElem.innerHTML = ''
    let idx = 0
    for (let task of tasks) {
        const dom = task.toDOM(idx++)
        tasksElem.appendChild(dom)
    }
}

function reset() {
    const before = document.querySelector('#beforeFld')
    before.value = new Date().toLocaleDateString()

    const text = document.querySelector('#textFld')
    text.value = ''
}

function setup() {
    const tasksDom = document.querySelector('#tasks')
    tasksDom.addEventListener('remove-todo', ev => {
        const ix = ev.detail.index
        tasks.splice(ix, 1)

        const dom = ev.detail.dom
        tasksDom.removeChild(dom)
    })

    const save = document.querySelector('#saveBtn')
    save.addEventListener('click', ev => {
        ev.preventDefault()
        const text = document.querySelector('#textFld')
        const before = document.querySelector('#beforeFld')

        if (text.value && before.value) {
            // todo: must POST this to server
            add(-1, text.value, before.value, false)
            populate()
            reset()
        }
    })
}

async function findAll() {
    const response = await fetch(baseUrl)
    const payload = await response.json()
    console.log('payload: %o', payload);

    tasks = payload.map(obj => Todo.fromServer(obj))
}


(async () => {
    setup()
    reset()
    await findAll()
    populate()
})()

