function weekdayOf(date) {
    if (!date) {
        date = new Date()
    } else if (typeof date === 'string') {
        date = new Date(date)
    }

    const WEEKDAYS = [
        'söndag', 'måndag', 'tisdag', 'onsdag', 'torsdag', 'fredag', 'lördag'
    ]
    return WEEKDAYS[date.getDay()]
}

const today = new Date(2020, 10, 9)
console.log('%s är en %s', today.toLocaleDateString(), weekdayOf(today))

const xmas = '2020-12-24'
console.log('%s är en %s', xmas, weekdayOf(xmas))

console.log('idag är det en %s', weekdayOf())
