'use strict'

const N = +process.argv[2] || 100

for (let k = 1; k <= N; ++k) {
    let txt = ''
    if (k % 3 === 0 && k % 5 === 0) {
        txt = 'FizzBuzz'
    } else if (k % 3 === 0) {
        txt = 'Fizz'
    } else if (k % 5 === 0) {
        txt = 'Buzz'
    }
    console.log('%d --> %s', k, txt);
}
