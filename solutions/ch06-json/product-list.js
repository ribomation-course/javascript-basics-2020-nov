const { readFileSync, writeFileSync } = require('fs')

function Fruit(name, price, inStock, date) {
    this.name = name
    this.price = price
    this.inStock = inStock
    this.date = date
}

function generateFruit() {
    const NAMES = ['apple', 'banana', 'coco nut', 'date plum']

    const name = NAMES[ Math.floor(NAMES.length * Math.random()) ]
    const price = 100 * Math.random()
    const inStock = Math.random() < 0.6
    const date = new Date(Date.now() - Math.floor(Math.random() * 30 * 24 * 3600 * 1000))

    return new Fruit(name, price, inStock, date)
}

function fmt(name, value) {
    if (name === 'date') { return value.slice(0, 10) }
    if (name === 'price') { return value.toFixed(2) }
    return value
}


const filename = 'fruits.json'
const N        = 5

const fruits = Array.from(Array(N).keys(), _ => generateFruit())
fruits.forEach(f => console.log(f))
writeFileSync(filename, JSON.stringify(fruits, fmt, 2), 'utf8')

console.log('--- Restored ---');
const restored = JSON.parse(readFileSync(filename).toString())
restored.forEach(f => console.log('%o', f))
