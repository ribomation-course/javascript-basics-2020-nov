function css(sel) {
    return document.querySelector(sel)
}

css('#geo').addEventListener('click', ev => {
    navigator.geolocation.getCurrentPosition(pos => {
        console.log('pos: %o', pos);
        css('#lat').innerText = pos.coords.latitude
        css('#lng').innerText = pos.coords.longitude
        css('#acc').innerText = pos.coords.accuracy
        css('#ts').innerText = new Date(pos.timestamp).toLocaleString()
    })
})

css('#showMap').addEventListener('click', ev => {
    navigator.geolocation.getCurrentPosition(pos => {
        const zoom = 18
        const lat = pos.coords.latitude
        const lng = pos.coords.longitude
        const url = `https://www.openstreetmap.org/#map=${zoom}/${lat}/${lng}`
        console.log('url: %s', url)

        //window.location.assign(url)
        const opts = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes"
        window.open(url, "Your current location", opts);
    })
})


