const { readFileSync } = require('fs')

class Car {
    id = 0
    producer = ''
    model = ''
    year = 0
    vin = ''
    city = ''

    static fromDB(payload) {
        return Object.assign(new Car(), payload)
    }

    toDB() {
        const obj = Object.assign({}, this)
        delete obj.id
        obj.updatedAt = new Date().toISOString()
        if (!obj.createdAt) { obj.createdAt = new Date().toISOString() }
        return obj
    }

    toString() {
        return `[${this.id}] ${this.producer} ${this.model} (${this.year}) @ ${this.city}. VIN=${this.vin}`
    }
}

console.log('fields of Car: %o', Object.keys(new Car()));

const filename = './cars.json'
const N = 10
const data = JSON.parse(readFileSync(filename).toString())
console.log('loaded %d records', data.length);

data.map(c => Car.fromDB(c))
    .sort((lhs, rhs) => rhs.year - lhs.year)
    .slice(0, N)
    .forEach(c => console.log('%s', c.toString()))

