import {sum} from './sum'

const N = +process.argv[2] || 10
console.log('SUM(1..%d) --> %d', N, sum(N))
