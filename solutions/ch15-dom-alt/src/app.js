
class Todo {
    text = ''
    before = new Date()
    done = false
    dom = undefined

    constructor(text, before = new Date()) {
        this.text = text
        if (before instanceof Date) {
            this.before = before
        } else if (typeof before === 'string') {
            this.before = new Date(before)
        } else {
            throw new Error('invalid date: ' + before + ', ' + typeof before)
        }
    }

    toggle() {
        this.done = !this.done
        return this.done
    }

    toDOM() {
        const article = document.createElement('article')
        article.innerHTML = `
            <h5>
            <span>${this.before.toDateString()}</span>
            <button>Done</button>
            <button>Remove</button>
            </h5>
            <p>${this.text}</p>
        `
        this.dom = article
        return article
    }
}

let tasks = []

function add(text, before) {
    tasks.push(new Todo(text, before))
    tasks.sort((lhs, rhs) => rhs.before.getTime() - lhs.before.getTime())
}

function populate() {
    const tasksEl = document.querySelector('#tasks')
    for (let task of tasks) {
        tasksEl.appendChild(task.toDOM())
    }
}


add('first task', '2020-11-27')
add('second task', '2020-11-25')
add('third task', '2020-11-30')
add('se kalle anka', '2020-12-24')
populate()
