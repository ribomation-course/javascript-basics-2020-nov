
let FACTOR = 10

function multiply(n) {
    return FACTOR * n
}

module.exports = {
    mult: multiply,
    set : (x) => {FACTOR = x}
}

