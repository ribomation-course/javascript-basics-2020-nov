let F = 10

module.exports = {
    mult: n => F * n,
    set: x => F = x
}
