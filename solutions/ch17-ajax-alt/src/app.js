
class Todo {
    id = 0
    text = ''
    before = new Date()
    done = false
    dom = undefined

    constructor(id, text, before = new Date(), done = false) {
        this.id = id
        this.text = text
        this.done = done
        if (before instanceof Date) {
            this.before = before
        } else if (typeof before === 'string') {
            this.before = new Date(before)
        } else {
            throw new Error('invalid date: ' + before + ', ' + typeof before)
        }
    }

    toggle() {
        this.done = !this.done
        return this.done
    }

    static fromServer(json) {
        return new Todo(json.id, json.text, json.before, json.done)
    }

    toDOM(idx) {
        const article = document.createElement('article')
        const self = this
        article.innerHTML = `
            <h5>
            <span>${this.before.toDateString()}</span>
            <button class="done">Done</button>
            <button class="remove">Remove</button>
            </h5>
            <p>${this.text}</p>
        `

        const doneBtn = article.querySelector('.done')
        doneBtn.addEventListener('click', ev => {
            self.toggle()
            if (self.done) self.dom.classList.add('done')
            else self.dom.classList.remove('done')
        })

        const removeBtn = article.querySelector('.remove')
        removeBtn.addEventListener('click', ev => {
            const removeEvt = new CustomEvent('remove-todo', {
                detail: {
                    index: idx,
                    dom: article
                }
            })
            const tasksEl = document.querySelector('#tasks')
            tasksEl.dispatchEvent(removeEvt)
        })

        this.dom = article
        return article
    }
}

const BASE_URL = 'http://localhost:3000/todos'
let tasks = []

function add(text, before) {
    tasks.push(new Todo(text, before))
    tasks.sort((lhs, rhs) => rhs.before.getTime() - lhs.before.getTime())
}

function populate() {
    const tasksEl = document.querySelector('#tasks')
    tasksEl.innerHTML = ''
    let idx = 0
    for (let task of tasks) {
        tasksEl.appendChild(task.toDOM(idx++))
    }
}

async function findAll() {
    const response = await fetch(BASE_URL)
    const payload = await response.json()
    console.log('*** payload: %o', payload);
    tasks = payload.map(obj => Todo.fromServer(obj))
}

function setup() {
    const tasksEl = document.querySelector('#tasks')
    tasksEl.addEventListener('remove-todo', ev => {
        tasks.splice(ev.detail.index, 1)
        tasksEl.removeChild(ev.detail.dom)
    })
    
    const save = document.querySelector('#saveBtn')
    save.addEventListener('click', ev => {
        const textFld = document.querySelector('#textFld')
        const beforeFld = document.querySelector('#beforeFld')
    
        if (textFld.value && beforeFld.value) {
            add(textFld.value, beforeFld.value)
            populate()
            textFld.value = beforeFld.value = ''
        }
    })
}
   

(async () => {
    try {
        setup()
        await findAll()
        populate()    
    } catch (err) {
        console.error(err);
    }
})()



